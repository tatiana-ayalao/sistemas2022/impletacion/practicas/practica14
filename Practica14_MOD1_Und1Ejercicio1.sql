﻿set NAMES 'utf8';
USE ciclistas;

/** Ejercicio1 Nombre y edad de los ciclistas que NO han ganado etapas  
  **/

 SELECT c.nombre, c.edad 
  FROM ciclista c LEFT JOIN etapa e ON c.dorsal = e.dorsal
  WHERE ((e.dorsal)IS NULL);


/** Ejercicio2 Nombre y edad de los ciclistas 
  que NO han ganado puertos  **/

  SELECT * FROM ciclista c 
    LEFT JOIN etapa e ON c.dorsal = e.dorsal
    WHERE (e.dorsal) IS NULL;



  /** Ejercicio 3 Listar el director de los equipos que tengan ciclistas NO que hayan ganado NINGUNA
etapa **/

SELECT DISTINCT e.director
  FROM equipo e
  INNER JOIN (ciclista c LEFT JOIN etapa e1 ON c.dorsal = e1.dorsal)
  ON e.nomequipo = c.nomequipo)
  WHERE etapa IS NULL ;


SELECT e.director
 FROM equipo e
LEFT JOIN (ciclista c LEFT JOIN etapa e1 ON c.dorsal = e1.dorsal) ON c.nomequipo = c.nomequipo
WHERE ((e1.dorsal)IS NULL);

  /** Ejercicio 4 Dorsal y nombre de los ciclistas que NO hayan 
  llevado algún maillot  **/

SELECT c.dorsal, c.nombre
 FROM ciclista c LEFT JOIN lleva l ON c.dorsal = l.dorsal
WHERE ((l.dorsal) IS NULL);



/** ejercicio 5 Dorsal y nombre de los ciclistas que NO hayan 
  llevado el maillot amarillo NUNCA **/

  SELECT DISTINCT l.dorsal 
  FROM maillot m INNER JOIN lleva l ON m.código = l.código
  WHERE ((maillot)="amarillo");

SELECT c.nombre, c.dorsal 
  FROM ciclista c LEFT JOIN ciclista c1= dorsal
  WHERE ((c.dorsal) IS NULL);  




  /**Consulta 6  Indicar el numetapa de las etapas que NO tengan
    puertos   **/

    SELECT * FROM etapa e LEFT JOIN puerto p ON e.numetapa= p.numetapa
      WHERE ((p.numetapa) IS NULL);



    /**  Consulta 7 Indicar la distancia media de las etapas 
      que NO tengan puertos   **/

SELECT AVG(e.kms) AS PromedioDekms 
 FROM etapa e LEFT JOIN puerto p ON e.numetapa=p.numetapa
WHERE((p.numetapa) IS NULL);

SELECT AVG(e.kms) AS PromedioDekms 
 FROM etapa e INNER JOIN e.numetapa=p.numetapa
WHERE((p.numetapa) IS NULL);





    /** CONSULTA 8  Listar el número de ciclistas que NO hayan ganado alguna etapa
   **/
SELECT COUNT(c.dorsal) AS CuentaDorsal 
  FROM ciclista c LEFT JOIN etapa e ON c.dorsal = e.dorsal
  WHERE((e.dorsal)IS NULL);

SELECT COUNT(c.dorsal) AS CuentaDorsal 
FROM ciclista c;



    /**   Consulta 9 Listar el dorsal de los ciclistas que hayan ganado alguna
      etapa que no tenga puerto  **/

SELECT DISTINCT c.dorsal 
  FROM (ciclista c INNER JOIN etapa e ON c.dorsal = e.dorsal)
  LEFT JOIN puerto p ON e.numetapa = p.numetapa
  WHERE((p.numetapa)IS NULL);




 /** Consulta 10 Listar el dorsal de los ciclistas 
  que hayan ganado únicamente etapas que no tengan
puertos     **/

SELECT * FROM ciclista c INNER JOIN (etapa LEFT JOIN
  puerto p ON etapa.numetapa = p.numetapa) ON c.dorsal=etapa.dorsal
  WHERE((p.numetapa)IS NULL);

SELECT DISTINCT c.dorsal 
  FROM (ciclista c INNER JOIN etapa e ON c.dorsal = e.dorsal)
  INNER JOIN puerto p ON e.numetapa = p.numetapa;



